import 'package:json_annotation/json_annotation.dart';
part 'Account.g.dart';

@JsonSerializable()
class Account{

  final int id;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name:'last_name')
  final String lastName;
  final String email;
  final String password;
  final String photo;
  @JsonKey(name: 'created_at', fromJson: _jsonToDateTime, toJson: _dateTimeToJson)
  final DateTime createdAt;
  @JsonKey(name: 'updated_at', fromJson: _jsonToDateTime, toJson: _dateTimeToJson)
  final DateTime updatedAt;

  const Account({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.password,
    this.photo,
    this.createdAt,
    this.updatedAt});

  factory Account.fromJson(Map<String, dynamic> json) => _$AccountFromJson(json);

  Map<String, dynamic> toJson() => _$AccountToJson(this);

  static DateTime _jsonToDateTime(String value) => value == null? null: DateTime.parse(value);
  static String _dateTimeToJson(DateTime value) => value == null? null: value.toIso8601String();

  @override
  String toString() {
    return 'Account{id: $id, firstName: $firstName, lastName: $lastName, email: $email, password: $password, photo: $photo, createdAt: $createdAt, updatedAt: $updatedAt}';
  }
}