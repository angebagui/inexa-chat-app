import 'package:flutter/material.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/models/Message.dart';
import 'package:inexa_chat_app/utils/constants.dart';
import 'package:intl/intl.dart';

import 'package:json_annotation/json_annotation.dart';
part 'Conversation.g.dart';

@JsonSerializable()
class Conversation{
 final int id;
 final List<int> speakers;
 @JsonKey(name:'is_group')
 final bool isGroup;
 @JsonKey(name:'group_name')
 final String groupName;
 final List<int> admins;
 @JsonKey(name:'speaker_list')
 final List<int> speakerList;
 final List<Account> users;
 final List<Message> messages;

 const Conversation({this.id, this.speakers, this.isGroup, this.groupName,
  this.admins, this.speakerList, this.users, this.messages});


 factory Conversation.fromJson(Map<String, dynamic> json) => _$ConversationFromJson(json);

 Map<String, dynamic> toJson() => _$ConversationToJson(this);

 Account getReceiver(Account me){

  if(users == null || users.isEmpty) return null;

  List<Account> items = users.where((element) => element.id != me.id).toList();

  if(items.isEmpty) return null;

  return items.first;

 }

 String title(Account me){
  if(isGroup){
   return groupName == null? 'Group inconnu':groupName;
  }else{
   final receiver = getReceiver(me);
   return receiver == null? 'Inconnu': "${receiver.firstName} ${receiver.lastName}";
  }
 }

 String avatar(Account me){
  if(isGroup){
   return "https://cdn4.iconfinder.com/data/icons/social-media-3/512/User_Group-512.png";
  }else{
   final receiver = getReceiver(me);
   return receiver == null || receiver.photo == null || receiver.photo.isEmpty? defaultAvatar:
   receiver.photo;
  }
 }

  String lastMessageContent() {

   if(messages == null || messages.isEmpty) return '';

   final message = messages.first;

   if(message.contentType == 'text')
    return message.content;
   else
    return 'Media envoyé';

  }

 String lastMessageTime(Locale locale) {

  if(messages == null || messages.isEmpty) return '';

  final message = messages.first;

  return DateFormat.Hm(locale.languageCode).format(message.createdAt);

 }

 int unReadMessagesCount() {

  if(messages == null || messages.isEmpty) return 0;

  final unReadMessages = messages.where((element) => element.isRead == false || element.isRead == null).toList();

  return unReadMessages.length;

 }

}