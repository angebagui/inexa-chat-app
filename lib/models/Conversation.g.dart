// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Conversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Conversation _$ConversationFromJson(Map<String, dynamic> json) {
  return Conversation(
    id: json['id'] as int,
    speakers: (json['speakers'] as List)?.map((e) => e as int)?.toList(),
    isGroup: json['is_group'] as bool,
    groupName: json['group_name'] as String,
    admins: (json['admins'] as List)?.map((e) => e as int)?.toList(),
    speakerList: (json['speaker_list'] as List)?.map((e) => e as int)?.toList(),
    users: (json['users'] as List)
        ?.map((e) =>
            e == null ? null : Account.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    messages: (json['messages'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ConversationToJson(Conversation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'speakers': instance.speakers,
      'is_group': instance.isGroup,
      'group_name': instance.groupName,
      'admins': instance.admins,
      'speaker_list': instance.speakerList,
      'users': instance.users,
      'messages': instance.messages,
    };
