// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Account _$AccountFromJson(Map<String, dynamic> json) {
  return Account(
    id: json['id'] as int,
    firstName: json['first_name'] as String,
    lastName: json['last_name'] as String,
    email: json['email'] as String,
    password: json['password'] as String,
    photo: json['photo'] as String,
    createdAt: Account._jsonToDateTime(json['created_at'] as String),
    updatedAt: Account._jsonToDateTime(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$AccountToJson(Account instance) => <String, dynamic>{
      'id': instance.id,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'email': instance.email,
      'password': instance.password,
      'photo': instance.photo,
      'created_at': Account._dateTimeToJson(instance.createdAt),
      'updated_at': Account._dateTimeToJson(instance.updatedAt),
    };
