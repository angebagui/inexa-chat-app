// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    id: json['id'] as int,
    content: json['content'] as String,
    senderId: json['sender_id'] as int,
    sender: json['sender'] == null
        ? null
        : Message.fromJson(json['sender'] as Map<String, dynamic>),
    conversationId: json['conversation_id'] as int,
    isRead: json['is_read'] as bool,
    isReceived: json['is_received'] as bool,
    isSent: json['is_sent'] as bool,
    contentType: json['content_type'] as String,
    createdAt: Message._jsonToDateTime(json['created_at'] as String),
    updatedAt: Message._jsonToDateTime(json['updated_at'] as String),
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'sender_id': instance.senderId,
      'sender': instance.sender,
      'conversation_id': instance.conversationId,
      'is_read': instance.isRead,
      'is_received': instance.isReceived,
      'is_sent': instance.isSent,
      'content_type': instance.contentType,
      'created_at': Message._dateTimeToJson(instance.createdAt),
      'updated_at': Message._dateTimeToJson(instance.updatedAt),
    };
