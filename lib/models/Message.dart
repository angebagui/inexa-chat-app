import 'package:json_annotation/json_annotation.dart';
part 'Message.g.dart';

@JsonSerializable()
class Message{

  final int id;
  final String content;
  @JsonKey(name: 'sender_id')
  final int senderId;
  final Message sender;
  @JsonKey(name: 'conversation_id')
  final int conversationId;
  @JsonKey(name: 'is_read')
  final bool isRead;
  @JsonKey(name: 'is_received')
  final bool isReceived;
  @JsonKey(name: 'is_sent')
  final bool isSent;
  @JsonKey(name: 'content_type')
  final String contentType;
  @JsonKey(name: 'created_at', fromJson: _jsonToDateTime, toJson: _dateTimeToJson)
  final DateTime createdAt;
  @JsonKey(name: 'updated_at', fromJson: _jsonToDateTime, toJson: _dateTimeToJson)
  final DateTime updatedAt;

  const Message({
    this.id,
    this.content,
    this.senderId,
    this.sender,
    this.conversationId,
    this.isRead,
    this.isReceived,
    this.isSent,
    this.contentType,
    this.createdAt,
    this.updatedAt});

  factory Message.fromJson(Map<String, dynamic> json) => _$MessageFromJson(json);

  Map<String, dynamic> toJson() => _$MessageToJson(this);


  static DateTime _jsonToDateTime(String value) => value == null? null: DateTime.parse(value);
  static String _dateTimeToJson(DateTime value) => value == null? null: value.toIso8601String();

  
}