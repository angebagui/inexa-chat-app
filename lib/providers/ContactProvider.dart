import 'package:flutter/material.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';

class ContactProvider extends ChangeNotifier{

  final BackendService _backendService;

  ContactProvider(this._backendService);

  List<Account> _allContacts = [];

  loadContacts(int accountID){
    _backendService.loadContactsByAccountID(accountID)
        .then((List<Account> value) => setData(value))
        .catchError((onError)=> print("Error found $onError"));
  }

  setData(List<Account> list){
    _allContacts = list;
    notifyListeners();
  }

  List<Account> get allContacts => _allContacts;

}