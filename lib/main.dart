import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inexa_chat_app/composition_root.dart';
import 'package:inexa_chat_app/utils/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';



void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  CompositionRoot.init();
  final Widget firstScreen  = await CompositionRoot.start();
  runApp(MyApp(firstScreen ));

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final Widget firstScreen;

  const MyApp(this.firstScreen);

  @override
  Widget build(BuildContext context) {

  
    return new MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en', ''), // English, no country code
        Locale('fr', ''), // French, no country code
      ],
      theme: lightTheme(),
      darkTheme: darkTheme(),
      home: firstScreen ,
      //initialRoute: '/login',
      /*routes: {
        '/login': (context)=> LoginScreen(),
        '/register': (context)=> RegisterScreen()
      } ,*/
    );
  }
}

