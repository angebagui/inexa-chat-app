import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inexa_chat_app/data/LocalStorageService.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';
import 'package:inexa_chat_app/state_management/register/register_state.dart';

class RegisterBloc extends Cubit<RegisterState>{
  final BackendService _backendService;
  final LocalStorageService _loginManager;
  RegisterBloc(this._backendService, this._loginManager) : super(RegisterInitial());

  Future<void> registration({String email, String password, String firstName, String lastName})async{

    emit(RegisterProgress());

    try{

      final account = await _backendService.createAccount(
          email: email,
          password: password,
          firstName: firstName,
          lastName: lastName);

      await _loginManager.store(account);

      emit(RegistrationSuccess(account));
    }catch(error){
      emit(RegistrationFailure(error));
    }
  }
}