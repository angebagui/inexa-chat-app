import 'package:inexa_chat_app/models/Account.dart';

abstract class RegisterState {}

class RegisterInitial extends RegisterState {}

class RegisterProgress extends RegisterState {}

class RegistrationSuccess extends RegisterState {
  final Account account;
  RegistrationSuccess(this.account);
}
class RegistrationFailure extends RegisterState {
  final dynamic error;
  RegistrationFailure(this.error);

}