import 'package:inexa_chat_app/models/Message.dart';

abstract class ConversationEvent {}

class FindConversation extends ConversationEvent{
  final List<int> speakers;
  FindConversation(this.speakers);
}

class FindMessagesByConversation extends ConversationEvent{

  final int conversationId;
  FindMessagesByConversation(this.conversationId);
}

class SendMessage extends ConversationEvent{
  Message message;
  SendMessage(this.message);
}

