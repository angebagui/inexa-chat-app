import 'package:inexa_chat_app/models/Account.dart';

abstract class LoginState{}

class LoginInitial extends LoginState{}
class LoginProgress extends LoginState{}
class LoginSuccess extends LoginState{
  final Account account;
  LoginSuccess(this.account);
}
class LoginFailed extends LoginState{
  final dynamic error;
  LoginFailed(this.error);
}