import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inexa_chat_app/data/LocalStorageService.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';
import 'package:inexa_chat_app/state_management/login/login_event.dart';
import 'package:inexa_chat_app/state_management/login/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState>{
  final BackendService _backendService;
  final LocalStorageService _loginManager;
  LoginBloc(this._backendService, this._loginManager) : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event)async* {

    if(event is LoginSubmitted){

      yield LoginProgress();

      try{
        final account = await _backendService.authenticate(email: event.email, password: event.password);
        await _loginManager.store(account);
        yield LoginSuccess(account);
      }catch(error){
        yield LoginFailed(error);
      }

    }

  }


}