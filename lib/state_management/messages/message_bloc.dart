import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inexa_chat_app/models/Conversation.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';

class MessageBloc extends Cubit<List<Conversation>>{
  final BackendService _backendService;

  MessageBloc(this._backendService) : super([]);

  void loadConversations(int accountID){

    _backendService.loadConversationsByAccountID(accountID)
    .then((value) =>  emit(value))
    .catchError((onError){
      print("Error found $onError");
    });

  }
}