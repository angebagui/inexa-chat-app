import 'package:flutter/material.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/providers/ContactProvider.dart';
import 'package:inexa_chat_app/state_management/messages/message_bloc.dart';
import 'package:inexa_chat_app/ui/widgets/ContactWidget.dart';
import 'package:provider/provider.dart';

class ListContactScreen extends StatefulWidget {

  final Account me;
  const ListContactScreen(this.me);

  @override
  _ListContactScreenState createState() => _ListContactScreenState();
}

class _ListContactScreenState extends State<ListContactScreen> {

  List<Account> list = [];
  @override
  void initState() {
    super.initState();

    final ContactProvider contactProvider = Provider.of<ContactProvider>(context,listen: false);
    contactProvider.loadContacts(widget.me.id);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des contacts"),
      ),
      body:Consumer<ContactProvider>(
        builder: (_, provider, child){
          return provider.allContacts == null || provider.allContacts.isEmpty? Container():
          ListView(
            children: provider.allContacts.map<ContactWidget>((contact) => ContactWidget(
                contact: contact,
                onClick: (){
                  _onContactClick(contact);
                }
            )).toList(),
          );
        },
      ),
    );
  }

  void _onContactClick(Account contact) {

    Navigator.pop(context, contact);

  }
}
