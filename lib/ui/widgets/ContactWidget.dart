import 'package:flutter/material.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/utils/constants.dart';

class ContactWidget extends StatelessWidget {

  final Account contact;
  final Function() onClick;
  const ContactWidget({
    @required this.contact,
    @required this.onClick
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onClick,
      leading:  Container(
        height: 60.0 ,
        width: 60.0,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(contact.photo == null || contact.photo.isEmpty? defaultAvatar: contact.photo)
            ),
            shape: BoxShape.circle
        ),
      ),
      title: Text("${contact.firstName} ${contact.lastName}", style: Theme.of(context).textTheme.headline6.copyWith(
          fontSize: 16

      ),),
      subtitle: Text(contact.email, style: Theme.of(context).textTheme.subtitle1,),
      trailing: Icon(Icons.arrow_forward_ios_outlined),
    );
  }

}
