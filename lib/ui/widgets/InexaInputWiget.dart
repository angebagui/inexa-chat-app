import 'package:flutter/material.dart';
import 'package:inexa_chat_app/utils/lang.dart';

class InexaInputWidget extends StatelessWidget {
  final String hintText;
  final TextInputType keyboardType;
  final bool obscureText;
  final TextEditingController controller;
  const InexaInputWidget({ @required this.hintText , this.keyboardType, this.obscureText, @required this.controller});

  @override
  Widget build(BuildContext context) {
    return TextField(
          decoration: InputDecoration(
            hintText: hintText??Lang.trans('input_default_title'),
            focusedBorder:OutlineInputBorder(borderRadius: BorderRadius.circular(10.0),borderSide: BorderSide(
              color: Theme.of(context).primaryColor
            )) ,
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
            //filled: true,
            fillColor: Colors.white,
      
          ),
          controller: controller,
          keyboardType: this.keyboardType??TextInputType.text,
          obscureText: obscureText??false,
          
        );
  }
}