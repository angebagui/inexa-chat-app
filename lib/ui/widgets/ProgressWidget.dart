import 'package:flutter/material.dart';

class ProgressWidget extends StatelessWidget {
  const ProgressWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Stack(
        children:[
                Container(
                  color:Colors.grey[300]
                ),
                Center(
                  child: CircularProgressIndicator(color: Colors.black,),
                )
        ]
    ),
    );
  }
}