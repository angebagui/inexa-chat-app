
import 'package:flutter/material.dart';

class InexaButton extends StatelessWidget {

  final String title;
  final Color color;
  final Function () onClick;
  const InexaButton({@required this.title, this.color, @required this.onClick});

  @override
  Widget build(BuildContext context) {
    return Container(
            margin: EdgeInsets.only(top: 20.0),
            width: MediaQuery.of(context).size.width,
            height: 60.0,
            child: ElevatedButton(
            onPressed: this.onClick,
            style: ButtonStyle(
              backgroundColor: color != null?MaterialStateProperty.all(color): MaterialStateProperty.all(Theme.of(context).primaryColor)
            ),
            child: Text(this.title??''),
          
          ),
        );
  }
}