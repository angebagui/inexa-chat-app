import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:inexa_chat_app/composition_root.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';
import 'package:inexa_chat_app/rest/BackendServiceImpl.dart';
import 'package:inexa_chat_app/rest/exception/AuthException.dart';
import 'package:inexa_chat_app/state_management/login/login_bloc.dart';
import 'package:inexa_chat_app/state_management/login/login_event.dart';
import 'package:inexa_chat_app/state_management/login/login_state.dart';
import 'package:inexa_chat_app/ui/auth/RegisterScreen.dart';
import 'package:inexa_chat_app/ui/widgets/InexaButton.dart';
import 'package:inexa_chat_app/ui/widgets/InexaInputWiget.dart';
import 'package:inexa_chat_app/ui/widgets/ProgressWidget.dart';
import 'package:inexa_chat_app/utils/InexaIcons.dart';
import 'package:inexa_chat_app/utils/Lang.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key key }) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  bool _isLoading = false;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

@override
  void initState() {
    super.initState();

    context.read<LoginBloc>().stream.listen((state) {
        print("LOG =>>>>> $state");
        //hideProgress();
        if(state is LoginSuccess){
          Toast.show("Utilisateur connecté", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
          Navigator.pushReplacement(context, MaterialPageRoute(
              builder: (context) => CompositionRoot.mainScreen(state.account)
          )
          );
        }
        if(state is LoginFailed){
          var  message = '';
          final error = state.error;
          if(error is AuthException){
            message = error.message;
          }

          if(error is Response){
            if(error.statusCode >= 500) {
              message = "Internal Server Error";
            }else{
              message = "Bad Response";
            }

          }

          if(error is SocketException){
            message = "Vérifiez votre connection internet !";
          }

          Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
        }
    });

  }
  @override
  void dispose() {
      _emailController?.dispose();
      _passwordController?.dispose();
    super.dispose();
  
  }

  @override
  Widget build(BuildContext context) {
      final lang = Localizations.localeOf(context).languageCode;
       Lang.setLanguage(lang.toUpperCase());
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Stack(
        children: [
          _buildBody(),
          Offstage(
            offstage: !_isLoading,
            child: ProgressWidget(),
          ),
        ],
      )
      );
  }

  void showProgress () => setState((){_isLoading = true;});
  void hideProgress () => setState((){_isLoading = false;});

  Widget _buildBody() => SafeArea(
        child: SingleChildScrollView(
        child: new Column(
          children: [

              _buildLogo(),
              _buildForm(),
          ],
        ),
      ),
    );

  Widget _buildLogo() => CircleAvatar(
    child: InexaIcons.app_logo,
    radius:100.0,
    backgroundColor: Colors.transparent,

  );

  Widget _buildForm() => Padding(
    padding: EdgeInsets.all(16.0),
    child: Column(
    children: [

        InexaInputWidget(
          hintText: Lang.trans('email_text'),
          keyboardType: TextInputType.emailAddress,
          controller: _emailController,
        ),
        SizedBox(height: 10,),
        InexaInputWidget(
          hintText: Lang.trans('password_text'),
          obscureText: true,
          controller:_passwordController
      
        ),

        BlocBuilder<LoginBloc,LoginState>(
            builder: (_, state){
              if(state is LoginProgress){
                return Container(
                  margin: EdgeInsets.only(top: 20.0),
                  width: MediaQuery.of(context).size.width,
                  height: 60.0,
                  color: Theme.of(context).primaryColor,
                  child: Center(
                    child: CircularProgressIndicator(color: Colors.black,),
                  ),
                );
              }else{
                return InexaButton(
                  title: Lang.trans('connexion_button_title'),
                  onClick: (){
                    _login();

                  },
                );
              }
            }
        ),
        InexaButton(
          title: Lang.trans('registration_button_title', 'Register'), 
          color: Theme.of(context).accentColor,
          onClick: (){
              print("Register");
             // Navigator.pushNamed(context, '/register');
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CompositionRoot.registerScreen()));
          },
          ),
        

    ],
  ),
  );

  void _login() {
    final String email = _emailController.text;
    final String password  =_passwordController.text;

    if(email.isEmpty){

      Toast.show("Email is required !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(password.isEmpty){

      Toast.show("Password is required !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(!email.contains('@')){

      Toast.show("Please enter valid email !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    print("Email: $email");
    print("Password: $password");
    //showProgress();



    context.read<LoginBloc>().add(LoginSubmitted(email, password));
   /* _backendService.authenticate(email: email, password: password)
    .then((Account value){
      hideProgress();
      if(value != null){
        print("Account found $value");
      }

    })
    .catchError((onError){
      hideProgress();

      print('Error found $onError');

    });*/



  }
}