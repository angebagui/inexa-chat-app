import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:inexa_chat_app/composition_root.dart';
import 'package:inexa_chat_app/rest/exception/AuthException.dart';
import 'package:inexa_chat_app/state_management/register/register_bloc.dart';
import 'package:inexa_chat_app/state_management/register/register_state.dart';
import 'package:inexa_chat_app/ui/widgets/InexaButton.dart';
import 'package:inexa_chat_app/ui/widgets/InexaInputWiget.dart';
import 'package:inexa_chat_app/utils/InexaIcons.dart';
import 'package:inexa_chat_app/utils/Lang.dart';
import 'package:inexa_chat_app/ui/auth/LoginScreen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen();

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();

    context.read<RegisterBloc>().stream.listen((state) {

      print("LOG =>>>>> $state");
      //hideProgress();
      if(state is RegistrationSuccess){
        Toast.show("Utilisateur connecté", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => CompositionRoot.mainScreen(state.account)
          )
        );

      }
      if(state is RegistrationFailure){
        var  message = '';
        final error = state.error;
        if(error is AuthException){
          message = error.message;
        }

        if(error is Response){
          if(error.statusCode >= 500) {
            message = "Internal Server Error";
          }else{
            message = "Bad Response";
          }

        }

        if(error is SocketException){
          message = "Vérifiez votre connection internet !";
        }

        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }

    });
  }

  @override
  void dispose() {
    _emailController?.dispose();
    _firstNameController?.dispose();
    _lastNameController?.dispose();
    _passwordController?.dispose();
    _confirmPasswordController?.dispose();

    super.dispose();

  }
  @override
  Widget build(BuildContext context) {
      final lang = Localizations.localeOf(context).languageCode;
       Lang.setLanguage(lang.toUpperCase());
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SafeArea(
        child: SingleChildScrollView(
        child: new Column(
          children: [

              _buildLogo(),
              _buildForm(),


          ],
        ),
      ),
    ),
      );
  }

  Widget _buildLogo() => CircleAvatar(
    child: InexaIcons.app_logo,
    radius:100.0,
    backgroundColor: Colors.transparent,

  );

  Widget _buildForm() => Padding(
    padding: EdgeInsets.all(16.0),
    child: Column(
    children: [

        InexaInputWidget(
          controller: _firstNameController,
          hintText: Lang.trans('firstname_text'),
        ),
         SizedBox(height: 10,),
        InexaInputWidget(
          controller: _lastNameController,
          hintText: Lang.trans('lastname_text'),
        ),
         SizedBox(height: 10,),
        InexaInputWidget(
          controller: _emailController,
          hintText: Lang.trans('email_text'),
        ),
        SizedBox(height: 10,),
        InexaInputWidget(
          controller: _passwordController,
          hintText: Lang.trans('password_text'),
        ),
         SizedBox(height: 10,),
        InexaInputWidget(
          controller: _confirmPasswordController,
          hintText: Lang.trans('confirm_password_text'),
        ),
        SizedBox(height: 20,),

      BlocBuilder<RegisterBloc,RegisterState>(
          builder: (_, state){

            print("State $state");
            if(state is RegisterProgress){
              return Container(
                margin: EdgeInsets.only(top: 20.0),
                width: MediaQuery.of(context).size.width,
                height: 60.0,
                color: Theme.of(context).primaryColor,
                child: Center(
                  child: CircularProgressIndicator(color: Colors.black,),
                ),
              );
            }else{
              return InexaButton(
                title: Lang.trans('registration_button_title'),
                onClick: (){
                  _register();

                },
              );
            }
          }
      ),
        InexaButton(
                 onClick: (){
              print("Login");
             // Navigator.pushNamed(context, '/register');
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CompositionRoot.loginScreen()));
          },
          title: Lang.trans('connexion_button_title'), color: Theme.of(context).accentColor,),
        

    ],
  ),
  );

  void _register() {
    final String email = _emailController.text;
    final String password  =_passwordController.text;

    if(email.isEmpty){

      Toast.show("Email is required !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(password.isEmpty){

      Toast.show("Password is required !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(!email.contains('@')){

      Toast.show("Please enter valid email !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(_firstNameController.text.isEmpty){

      Toast.show("Firstname is required !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(_lastNameController.text.isEmpty){

      Toast.show("Lastname is required !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    if(_confirmPasswordController.text.isEmpty){

      Toast.show("Confirm your password please !", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }

    context.read<RegisterBloc>().registration(
      email: _emailController.text,
      password: _passwordController.text,
      firstName: _firstNameController.text,
      lastName: _lastNameController.text
    ).whenComplete((){
      print("done");
    });

  }
}