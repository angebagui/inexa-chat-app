import 'package:flutter/material.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/models/Conversation.dart';
import 'package:inexa_chat_app/state_management/messages/message_bloc.dart';
import 'package:inexa_chat_app/ui/widgets/ConversationWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
class MessageScreen extends StatefulWidget {
  final Account me;
  const MessageScreen(this.me);

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {

  @override
  void initState() {
    super.initState();

   if(widget.me != null){
     context.read<MessageBloc>().loadConversations(widget.me.id);
   }

  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MessageBloc,List<Conversation>>(
        builder: (_, state){
          return state == null? Container():ListView(
            children: state.map<ConversationWidget>((conversation) => ConversationWidget(
              conversation: conversation,
              me: widget.me,
            )).toList()
          );
        }
    );
  }
}