import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:inexa_chat_app/composition_root.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/state_management/conversation/conversation_bloc.dart';
import 'package:inexa_chat_app/state_management/conversation/conversation_event.dart';
import 'package:inexa_chat_app/state_management/conversation/conversation_state.dart';
import 'package:inexa_chat_app/ui/conversation/ConversationScreen.dart';
import 'package:inexa_chat_app/ui/main/tabs/GroupScreen.dart';
import 'package:inexa_chat_app/utils/constants.dart';
import 'package:inexa_chat_app/utils/InexaIcons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toast/toast.dart';

class MainScreen extends StatefulWidget {
  final Account account;
  const MainScreen(this.account);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  int  _currentPosition = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    context.read<ConversationBloc>().stream.listen((state) {

      if(state is ConversationFound){

        final route = MaterialPageRoute(builder: (context)=> CompositionRoot.conversationScreen(state.conversation, widget.account));

        Navigator.push(context, route);
      }

      if(state is Failure){
        var  message = '';
        final error = state.error;

        if(error is Response){
          if(error.statusCode >= 500) {
            message = "Internal Server Error";
          }else{
            message = "Bad Response";
          }

        }

        if(error is SocketException){
          message = "Vérifiez votre connection internet !";
        }

        Toast.show(message, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2, 
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: Text(title),
          bottom: TabBar(
            tabs: [

                Tab(text: 'Message',),
                Tab(text: 'Group',),

            ],
          ),

          ),
        body: TabBarView(
          children: [
               CompositionRoot.messageScreen(widget.account),
               GroupScreen()

          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: ()async{

           final Account contact = await Navigator.push(context, MaterialPageRoute(builder: (_)=> CompositionRoot.listContactScreen(widget.account)));
           print("Selected user: $contact");
           if(contact != null){
             _openConversation(contact);
           }
          }
        ) ,
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentPosition,
          items: [
            BottomNavigationBarItem(
              icon: InexaIcons.message,
              activeIcon: InexaIcons.message_active,
              label: '',
            ) ,
            BottomNavigationBarItem(
              icon: InexaIcons.friends,
              activeIcon: InexaIcons.friends_active,
              label: ''
            ) ,
            BottomNavigationBarItem(
              icon: InexaIcons.add_box,
              activeIcon: InexaIcons.add_box_active,
              label: ''
            ),
            BottomNavigationBarItem(
              icon: InexaIcons.notifications,
              activeIcon: InexaIcons.notifications_active,
              label: ''
            ),
            BottomNavigationBarItem(
              icon: InexaIcons.profile,
              activeIcon: InexaIcons.profile_active,

              label: ''
            )    
          ],
          onTap: (int position){
            print("Position $position");
            
             print("CurrentPosition $_currentPosition");

             setState(() {
               _currentPosition = position;
             });
          },
        ),
      )
    );
  }

  void _openConversation(Account contact) {

    final speakers = [widget.account.id, contact.id];
    context.read<ConversationBloc>().add(FindConversation(speakers));

  }
}