import 'package:flutter/material.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/models/Conversation.dart';
import 'package:inexa_chat_app/models/Message.dart';

abstract class BackendService{

  Future<Account> createAccount({@required String email, @required String password, @required String firstName, @required String lastName});
  Future<Account> authenticate({@required String email, @required String password});
  Future<Conversation> findOrCreateConversation({@required List<int> speakers, String groupName, @required bool  isGroup, List<int> admins});
  Future<List<Conversation>> loadConversationsByAccountID(int accountID);
  Future<List<Message>> loadMessagesByConversationByID(int conversationID);
  Future<Message> sendMessage({@required String content, @required String contentType, @required int senderId, @required int conversationId,});
  Future<List<Account>> loadContactsByAccountID(int accountID);

}