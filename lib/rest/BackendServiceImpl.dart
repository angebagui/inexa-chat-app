import 'dart:convert';

import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/models/Conversation.dart';
import 'package:inexa_chat_app/models/Message.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';
import 'package:http/http.dart';
import 'package:inexa_chat_app/rest/exception/AuthException.dart';

class BackendServiceImpl implements BackendService{

  final String API_BASE_URL =  "https://socialapp.adjemin.com/api/v1/";

  @override
  Future<Account> authenticate({String email, String password})async {
    final Uri url = Uri.parse("${API_BASE_URL}user_auth");
    print("$url");
    final response = await post(url,body: {
      'email':email,
      'password':password
    },
    headers: {
      'Accept':'application/json',
      'Content-Type':'application/x-www-form-urlencoded'
    }
    );

    if(response.statusCode == 200 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final Map jsonData = jsonBody['data'];
        final Account account = Account.fromJson(jsonData);
        return account;
      }else{
        throw AuthException(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw AuthException(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }

  @override
  Future<Account> createAccount({String email, String password, String firstName, String lastName})async {
    final Uri url = Uri.parse("${API_BASE_URL}user_register");
    print("$url");
    final response = await post(url,body: {
      'first_name':firstName,
      'last_name':lastName,
      'email':email,
      'password':password
    },
        headers: {
          'Accept':'application/json',
          'Content-Type':'application/x-www-form-urlencoded'
        }
    );

    if(response.statusCode == 200 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final Map jsonData = jsonBody['data'];
        final Account account = Account.fromJson(jsonData);
        return account;
      }else{
        throw AuthException(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw AuthException(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }

  @override
  Future<Conversation> findOrCreateConversation({List<int> speakers, String groupName, bool isGroup, List<int> admins}) async{
    final Uri url = Uri.parse("${API_BASE_URL}open_conversation");
    print("$url");
    final Map requestJsonBody = <String, dynamic>{
      "speakers": speakers,
      "is_group":isGroup,
      "group_name": groupName,
      "admins":admins
    };

    final response = await post(url,
        body: jsonEncode(requestJsonBody),
        headers: {
          'Accept':'application/json',
          'Content-Type':'application/json'
        }
    );

    if((response.statusCode == 200 || response.statusCode == 201) && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final Map jsonData = jsonBody['data'];
        final Conversation data = Conversation.fromJson(jsonData);
        return data;
      }else{
        throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }

  @override
  Future<List<Account>> loadContactsByAccountID(int accountID) async{
    final Uri url = Uri.parse("${API_BASE_URL}contacts/$accountID");
    print("$url");
    final response = await get(url,
        headers: {
          'Accept':'application/json'
        }
    );

    if(response.statusCode == 200 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final List jsonData = jsonBody['data'] as List;
        final List<Account> data = jsonData.map<Account>((e) => Account.fromJson(e as Map)).toList();
        return data;
      }else{
        throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }

  @override
  Future<List<Conversation>> loadConversationsByAccountID(int accountID) async{
    final Uri url = Uri.parse("${API_BASE_URL}conversations/customers/$accountID");
    print("$url");
    final response = await get(url,
        headers: {
          'Accept':'application/json'
        }
    );

    if(response.statusCode == 200 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final List jsonData = jsonBody['data'] as List;
        final List<Conversation> data = jsonData.map<Conversation>((e) => Conversation.fromJson(e as Map)).toList();
        return data;
      }else{
        throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }

  @override
  Future<List<Message>> loadMessagesByConversationByID(int conversationID)async{
    final Uri url = Uri.parse("${API_BASE_URL}conversations/messages/$conversationID");
    print("$url");
    final response = await get(url,
        headers: {
          'Accept':'application/json'
        }
    );

    if(response.statusCode == 200 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final List jsonData = jsonBody['data'] as List;
        final List<Message> data = jsonData.map<Message>((e) => Message.fromJson(e as Map)).toList();
        return data;
      }else{
        throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }

  @override
  Future<Message> sendMessage({String content, String contentType, int senderId, int conversationId}) async{
    final Uri url = Uri.parse("${API_BASE_URL}messages");
    print("$url");
    final response = await post(url,
        body: <String, String>{
          "content": content,
          "content_type":contentType,
          "sender_id": "$senderId",
          "conversation_id":"$conversationId"
        },
        headers: {
          'Accept':'application/json',
          'Content-Type':'application/x-www-form-urlencoded'
        }
    );

    if(response.statusCode == 200 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      if(jsonBody.containsKey('success') && jsonBody['success'] == true && jsonBody.containsKey('data')){
        final Map jsonData = jsonBody['data'];
        final Message data = Message.fromJson(jsonData);
        return data;
      }else{
        throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
      }

    }else if(response.statusCode >= 400 && response.statusCode < 500 && response.headers['content-type'] == 'application/json'){
      final Map jsonBody = jsonDecode(response.body);
      throw Exception(jsonBody.containsKey('message')?jsonBody['message']:'Internal Server Error');
    }else{
      throw response;
    }
  }


}