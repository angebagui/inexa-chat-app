class Lang{

 static String  _defaulLangage = 'FR';

  static Map<String, String > _frenchList = {
      'connexion_button_title' : 'Connexion',
      'input_default_title': 'Champ de saisi',
      'email_text':'Adresse électronique',
      'password_text':'Mot de passe',
      'registration_button_title':'Inscription',
      'firstname_text':'Prénom',
      'lastname_text':'Nom',
      'confirm_password_text':'Confirmation Mot de passe'
  };

  static Map<String, String > _englishList = {
      'connexion_button_title' : 'Login',
      'input_default_title': 'Input',
      'email_text':'Email',
      'password_text':'Password',
      'registration_button_title':'Register',
      'firstname_text':'Firstname',
      'lastname_text':'Lastname',
      'confirm_password_text':'Confirm Password'
  };

  static String trans(String key, [String defaultValue ='']){

    if(_defaulLangage == 'FR'){
      if(!_frenchList.containsKey(key)){
          return defaultValue.isEmpty?key:defaultValue;
      }else{
        return _frenchList[key];
      }
    
    }else{
       if(!_englishList.containsKey(key)){
            return defaultValue.isEmpty?key:defaultValue;
      }else{
        return _englishList[key];
      }
    }
  }

  static void setLanguage(String lang){
     _defaulLangage = lang;
  }

  String getLanguage(){
    return  _defaulLangage;
  }

}