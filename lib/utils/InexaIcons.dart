import 'package:flutter/material.dart';
import 'package:inexa_chat_app/utils/InexaColors.dart';

class InexaIcons{
    
    static final Widget message = Image.asset('assets/images/message.png', color: Color(0xFF91A7C0),);
    static final Widget friends = Image.asset('assets/images/friends.png');
    static final Widget add_box = Image.asset('assets/images/add_box.png');
    static final Widget notifications = Image.asset('assets/images/notifications.png');
    static final Widget profile = Image.asset('assets/images/profile.png');

    static final Widget message_active = Image.asset('assets/images/message.png', color: InexaColors.primaryColor,);
    static final Widget friends_active = Image.asset('assets/images/friends.png', color: InexaColors.primaryColor,);
    static final Widget add_box_active = Image.asset('assets/images/add_box.png', color: InexaColors.primaryColor);
    static final Widget notifications_active = Image.asset('assets/images/notifications.png', color: Colors.black,);
    static final Widget profile_active = Image.asset('assets/images/profile.png', color: InexaColors.primaryColor,);


     static final Widget app_logo = Image.asset('assets/images/logo_inexa.png');
}