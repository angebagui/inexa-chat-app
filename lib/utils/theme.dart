
import 'package:flutter/material.dart';
import 'package:inexa_chat_app/utils/InexaColors.dart';

ThemeData lightTheme () => ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor: InexaColors.primaryColor,
        accentColor: InexaColors.accentColor,
        backgroundColor: Colors.white,
        fontFamily: "SFPro"
      );

ThemeData darkTheme () => ThemeData(
  // This is the theme of your application.
  //
  // Try running your application with "flutter run". You'll see the
  // application has a blue toolbar. Then, without quitting the app, try
  // changing the primarySwatch below to Colors.green and then invoke
  // "hot reload" (press "r" in the console where you ran "flutter run",
  // or simply save your changes to "hot reload" in a Flutter IDE).
  // Notice that the counter didn't reset back to zero; the application
  // is not restarted.
  primaryColor: Colors.red,
  accentColor: Colors.white,
  backgroundColor: Colors.black,
   fontFamily: "SFPro"
);