import 'package:flutter/material.dart';

class InexaColors{

  static final  Color primaryColor = Colors.red;
   static final Color accentColor = Colors.black;
  static final Color conversationTimeColor = Color(0xFF9F9F9F);
  static final Color onlineUserColor= Color(0xFF00F86B);
 
}