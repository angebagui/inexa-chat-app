import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inexa_chat_app/data/DatabaseManager.dart';
import 'package:inexa_chat_app/data/LocalStorageService.dart';
import 'package:inexa_chat_app/data/SharedPrefs.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:inexa_chat_app/models/Conversation.dart';
import 'package:inexa_chat_app/providers/ContactProvider.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';
import 'package:inexa_chat_app/rest/BackendServiceImpl.dart';
import 'package:inexa_chat_app/state_management/conversation/conversation_bloc.dart';
import 'package:inexa_chat_app/state_management/login/login_bloc.dart';
import 'package:inexa_chat_app/state_management/messages/message_bloc.dart';
import 'package:inexa_chat_app/state_management/register/register_bloc.dart';
import 'package:inexa_chat_app/ui/auth/LoginScreen.dart';
import 'package:inexa_chat_app/ui/auth/RegisterScreen.dart';
import 'package:inexa_chat_app/ui/contacts/ListContactScreen.dart';
import 'package:inexa_chat_app/ui/conversation/ConversationScreen.dart';
import 'package:inexa_chat_app/ui/main/MainScreen.dart';
import 'package:inexa_chat_app/ui/main/tabs/MessageScreen.dart';
import 'package:provider/provider.dart';

class CompositionRoot{

  static BackendService  _backendService;
  static LocalStorageService _localStorageService;

  static init(){
    _backendService = BackendServiceImpl();
    _localStorageService = DatabaseManager();
  }

  static Future<Widget> start()async{
    final Account account = await _localStorageService.connectedUser();

    return account == null? loginScreen(): mainScreen(account);
  }


  static Widget loginScreen(){
    final LoginBloc _loginBloc = LoginBloc(_backendService, _localStorageService);
    return BlocProvider(
      create: (_) => _loginBloc,
      child: LoginScreen(),
    );
  }

  static Widget registerScreen(){
    final RegisterBloc _registerBloc = RegisterBloc(_backendService, _localStorageService);
    return BlocProvider(
      create: (_) => _registerBloc,
      child: RegisterScreen(),
    );
  }

  static Widget mainScreen(Account account){

    final ConversationBloc _conversationBloc = ConversationBloc(_backendService);
    return BlocProvider(
      create: (_) => _conversationBloc,
      child: MainScreen(account),
    );
  }
  static Widget conversationScreen(Conversation conversation, Account account){

    final ConversationBloc _conversationBloc = ConversationBloc(_backendService);
    return BlocProvider(
      create: (_) => _conversationBloc,
      child: ConversationScreen(conversation,account),
    );
  }

  static Widget messageScreen(Account account){
    final MessageBloc _registerBloc = MessageBloc(_backendService);
    return BlocProvider(
      create: (_) => _registerBloc,
      child: MessageScreen(account),
    );
  }

  static Widget listContactScreen(Account account){

    return ChangeNotifierProvider<ContactProvider>(
     create: (_) => ContactProvider(_backendService),
      child: ListContactScreen(account),
    );
  }

}