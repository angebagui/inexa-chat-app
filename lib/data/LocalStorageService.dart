import 'package:inexa_chat_app/models/Account.dart';

abstract class LocalStorageService{

  Future<void> store(Account account);
  Future<Account> connectedUser();

}