import 'dart:convert';

import 'package:inexa_chat_app/data/LocalStorageService.dart';
import 'package:inexa_chat_app/database/DatabaseHelper.dart';
import 'package:inexa_chat_app/models/Account.dart';

class DatabaseManager extends LocalStorageService{

  /**
   * Enregistrer un utilisateur
   */
  @override
  Future<void> store(Account account) async{

    //Supprimer tous les utilisateurs déjà dans la table ACCOUNTS
    await DatabaseHelper.deleteUsers();

    final Map<String, dynamic> json = {
      "data":jsonEncode(account.toJson())
    };

    //Insérer une ligne dans la table ACCOUNTS
    await DatabaseHelper.insertUser(json);

  }

  /**
   * Récureperer l'utilisateur connecté
   */
  @override
   Future<Account> connectedUser()async{

    final int limit = 1;
    final List<Map<String, dynamic>> rows = await DatabaseHelper.allUsers(limit);
    if(rows != null && rows.isNotEmpty){
      final Map<String, dynamic> firstElement = rows.first;
      /**
       * {
       *  'id':1,
       *  'data':{
            "id": 75,
            "first_name": "Test",
            "last_name": "Test",
            "email": "angebagui@adjemin.com",
            "password": "$2y$10$QmhkdrpA2zQ5Jcuq0TTxveCAkIcbwSCN8qaMMJtxG.SHwQkEtRPSO",
            "photo": "https://thumbs.dreamstime.com/b/ic-ne-masculine-de-photo-de-profil-d-avatar-de-d%C3%A9faut-texte-d-attente-gris-de-photo-d-homme-88414414.jpg",
            "created_at": "2021-09-22T12:25:09.000000Z",
            "updated_at": "2021-09-22T12:25:09.000000Z",
            "deleted_at": null
          }
       * }
       */
      final Map<String, dynamic> data = jsonDecode(firstElement['data']);
      return Account.fromJson(data);

    }else{
      return null;
    }


  }


}