import 'dart:convert';
import 'package:inexa_chat_app/data/LocalStorageService.dart';
import 'package:inexa_chat_app/models/Account.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs extends LocalStorageService{

  SharedPrefs();

  @override
  Future<Account> connectedUser()async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final String jsonString  = prefs.getString('inexa_chat_app.USER_CONNECTED');
    final Map json = jsonString == null || jsonString.isEmpty ? null: jsonDecode(jsonString);
    return json == null? null: Account.fromJson(json);
  }

  @override
  Future<void> store(Account account) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('inexa_chat_app.USER_CONNECTED', jsonEncode(account.toJson()));
  }



}