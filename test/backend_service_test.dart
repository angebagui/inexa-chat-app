import 'package:flutter_test/flutter_test.dart';
import 'package:inexa_chat_app/rest/BackendService.dart';
import 'package:inexa_chat_app/rest/BackendServiceImpl.dart';
import 'package:inexa_chat_app/rest/exception/AuthException.dart';

void main(){

  BackendService  backendService = BackendServiceImpl();
  
 /* test('login user if bool', ()async{

    final  resp = await backendService.authenticate(email: 'angebagui@adjemin.com', password: '123456789');
    print('Response: $resp');
    expect(resp, true);

  });*/

  test('login user if Account is not null', ()async{
    final  resp = await backendService.authenticate(email: 'angebagui@adjemin.com', password: '123456789');
    print('Response: $resp');
    expect(resp != null, true);

  });

  test('when invalid credentials', ()async{
    try{
      final  resp = await backendService.authenticate(email: 'angebagui@ain.com', password: '1234589');
      print('Response: $resp');

    }catch(error){
      expect(error is AuthException, true);
    }

  });
  
}